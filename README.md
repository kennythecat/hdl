# Hardware Description Language

## TOC

- [Verilator](#verilator)

***

## Description

Include the Verilator(HDL-simulator) installation guide and example code

## Verilator

Open source, faster, using C++ as supported.

***

## Revision history : initial
