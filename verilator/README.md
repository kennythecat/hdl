# Verilator Guide

## Table of Contents

- [Verilator Guide](#verilator-guide)
  - [Table of Contents](#table-of-contents)
  - [Git Quick Install](#git-quick-install-verilator)
  - [Install GTKWave](#install-gtkwave)
  - [Try a Example](#try-a-example)
  - [File Structure](#file-structure)

 ***

## Git Quick Install Verilator

You can follow the [Official Website](https://veripool.org/guide/latest/install.html)

Or : 

``` bash
git clone https://github.com/verilator/verilator   # Only first time

# Every time you need to build:
unsetenv VERILATOR_ROOT  # For csh; ignore error if on bash
unset VERILATOR_ROOT  # For bash
cd verilator
git pull         # Make sure git repository is up-to-date
git tag          # See what versions exist
#git checkout master      # Use development branch (e.g. recent bug fixes)
#git checkout stable      # Use most recent stable release
#git checkout v{version}  # Switch to specified release version

autoconf         # Create ./configure script
./configure      # Configure and create Makefile
make -j `nproc`  # Build Verilator itself (if error, try just 'make')
sudo make install
export PATH="/usr/local/bin:$PATH”  # Do this, if in wsl

```

Check the version:

```verilator --version```

## Install GTKWave

```sudo apt install gtkwave```

## Try a example

Compile

```bash
verilator -Wall adder.sv sim_main.cpp --cc --trace --exe --build

```

Run the file

```./obj_dir/Vadder```

result:

```bash
a = 1, b = 0, sum = 1
a = 1, b = 1, sum = 2
a = 1, b = 1, sum = 2
a = 0, b = 0, sum = 0
a = 1, b = 1, sum = 2
a = 0, b = 1, sum = 1
a = 0, b = 1, sum = 1
a = 1, b = 0, sum = 1
a = 0, b = 0, sum = 0
a = 0, b = 0, sum = 0
a = 1, b = 0, sum = 1
a = 1, b = 1, sum = 2
a = 0, b = 0, sum = 0
a = 0, b = 1, sum = 1
a = 1, b = 1, sum = 2
a = 1, b = 0, sum = 1
a = 0, b = 0, sum = 0
a = 1, b = 1, sum = 2
a = 1, b = 0, sum = 1
a = 1, b = 0, sum = 1
a = 1, b = 1, sum = 2
```

Check the Waveform

```gtkwave wave.vcd```

![wave](image/wave.png)

***

## File Structure

``` bash
- adder.v
- guide.md
- sim_main.cpp
```

## Revision Version: Initial

Author: Kenny
