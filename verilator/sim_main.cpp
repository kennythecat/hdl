#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "Vadder.h"  //adder.v will be compiled as Vadder.h
#include "verilated.h"

#include "verilated_vcd_c.h" // gen the VCD file

int main(int argc, char** argv) {
    VerilatedContext* contextp = new VerilatedContext;
    contextp->commandArgs(argc, argv);
    Vadder* adder = new Vadder{contextp};


    VerilatedVcdC* tfp = new VerilatedVcdC; //initial VCD pointer
    contextp->traceEverOn(true); //active trace function
    adder->trace(tfp, 0); //
    tfp->open("wave.vcd"); // output VCD file

    int i=20;
    while (!contextp->gotFinish() && i>=0) {
        int a = rand() & 1;
        int b = rand() & 1;
        adder->a = a;
        adder->b = b;
        adder->eval();
        printf("a = %d, b = %d, sum = %d\n", a, b, adder->sum);

        tfp->dump(contextp->time()); //dump wave
        contextp->timeInc(1); // push simulation time

        assert(adder->sum == a + b);

        i--;
    }
    delete adder;
    tfp->close();
    delete contextp;
    return 0;
}